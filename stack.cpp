﻿#include <string>
#include <iostream>

class Stack
{
public:
    Stack() // конструктор без параметров
    {
        arr = new int[0];
        size = 0;
        capacity = 0;
    }
    Stack(int n) // конструктор с одним параметром
    {
        arr = new int[0];
        size = 0;
        capacity = n;
    }
    void push(int val) // добавление элемента в стек
    {
        if (size == capacity)
        {
            resize();
        }
        arr[size] = val;
        size++;
    }
    void pop() //удаление элемента из стека
    {
        if (size == 0)
        {
            throw std::exception("Stack is empty!");
        }
        size--;
    }
    bool is_empty() // проверка на пустоту стека
    {
        if (size == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    int front_el() // верхний элемент стека
    {
        if (size == 0)
        {
            throw std::exception("Stack is empty!");
        }
        return arr[size - 1];
    }
    void print() //вывод элементов стека
    {
        if (size == 0)
        {
            throw std::exception("Stack is empty!");
        }
        for (int i = 0; i < size; i++)
        {
            std::cout << arr[i] << " ";
        }
        std::cout << std::endl;
    }
private:
    int* arr;
    int size;
    int capacity;
    void resize()
    {
        if (size == 0)
        {
            capacity++;
            arr = new int[1];
            return;
        }
        int* arr1 = new int[size * 2];
        for (int i = 0; i < size; i++)
        {
            arr1[i] = arr[i];
        }
        delete arr;
        arr = arr1;
        capacity *= 2;
    }
};

int main()
{
    std::cout << "1. create stack" << std::endl << "2. create stack with capacity" << std::endl << "3. add value" << std::endl << "4. delete value"
        << std::endl << "5. is stack empty" << std::endl << "6. front element" << std::endl << "7. print stack" << std::endl << "8. end programme" << std::endl;
    std::string str;
    Stack* stack;
    getline(std::cin, str);
    if (str == "create stack with capacity")
    {
        int n;
        std::cin >> n;
        stack = new Stack(n);
    }
    else
    {
        stack = new Stack();
    }
        do
        {
            getline(std::cin, str);
            if (str == "add value")
            {
                int val;
                std::cin >> val;
                stack->push(val);
            }
            else if (str == "delete value")
            {
                try
                {
                    stack->pop();
                }
                catch (std::exception ex)
                {
                    std::cout << ex.what() << std::endl;
                }
            }
            else if (str == "is stack empty")
            {
                if (stack->is_empty())
                {
                    std::cout << "Stack is empty." << std::endl;
                }
                else
                {
                    std::cout << "Stack is not empty." << std::endl;
                }
            }
            else if (str == "front element")
            {
                try
                {
                    std::cout << stack->front_el() << std::endl;
                }
                catch (std::exception ex)
                {
                    std::cout << ex.what() << std::endl;
                }
            }
            else if (str == "print stack")
            {
                try
                {
                    stack->print();
                }
                catch (std::exception ex)
                {
                    std::cout << ex.what() << std::endl;
                }
            }
        }while(str != "end programme");
    system("pause");
}